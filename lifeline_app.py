from flask import Flask
app = Flask(__name__)

import requests
import schedule
import time
import json

currentWaypoint = -1

while (1 == 1):
    print('requesting waypoint')
    payload = '{"currentWaypoint": '+str(currentWaypoint)+', "currentLat": "37.648176", "currentLon": "-91.562598" }'
    r = requests.post("https://api.backendless.com/C2329A15-4F80-8917-FF4E-E92008829F00/AA185700-AF20-C1C0-FF60-0E97EAA63B00/services/CustomService/checkForInstructions", data=payload)
    jsonData = r.json()
    print(r.text)
    
    currentWaypoint = jsonData["waypointNumber"]
    specialInstructions = jsonData["specialInstructions"]
    waypointData = jsonData["waypointData"]
    
    with open('nextWaypoint.txt', 'w') as outfile:
        json.dump(jsonData, outfile)

