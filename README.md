# README #

The LifeLine - Flask Server handles communication from the Flask server on the vehicle to the Backendless server controlling events and waypoints waiting to be delivered to the vehicle.

### How do I get set up? ###

This code assumes the base installation of Flask is setup and contains the following dependancies:

**Schedule**  
`pip install schedule`

**Requests**  
`pip install requests`
